var alunos = [];

const main = () => {
  const x = confirm("Deseja cadastrar alunos no sistema?");
  if (x === true) {
    cadastro();
  }
};

const validar_alunos = () => {
  const filtrados = alunos.filter((val) => val.nota < 60);
  return filtrados;
};

const reajuste = () => {
  let notas_ajustadas = alunos.map((item) => {
    if (item.bonus === true && item.nota >= 55 && item.nota < 60) {
      item.nota = 60;
    }
    if (item.bonus === true && item.nota > 60) {
      item.nota = item.nota + item.nota * 0.1;
      if (item.nota > 100) {
        item.nota = 100;
      }
    }
    return item;
  });
  return notas_ajustadas;
};

const cadastro = () => {
  const nome_aluno = prompt("Informe o nome do aluno");
  let nota_aluno = parseFloat(prompt("Informe a nota do aluno"));
  const bonus_aluno = confirm("O aluno possui bônus na nota?");

  const aluno = {
    nome: nome_aluno,
    nota: nota_aluno,
    bonus: bonus_aluno,
  };

  alunos.push(aluno);

  const y = confirm("Deseja cadastrar mais alunos?");

  if (y === true) {
    cadastro();
  } else {
    let notas_ajustadas = reajuste();
    const filtrados = validar_alunos();

    let resposta = "Todos os alunos: \n";

    for (const item of notas_ajustadas) {
      resposta =
        resposta +
        `
    Nome: ${item.nome} | Nota: ${item.nota}
`;
    }
    resposta =
      resposta +
      `
Alunos reprovados:
    `;
    for (const item of filtrados) {
      resposta =
        resposta +
        `
    Nome: ${item.nome} | Nota: ${item.nota}
`;
    }

    console.log(resposta);
  }
};

main();
