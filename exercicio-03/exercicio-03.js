let nome = "Antonio Alberto";
let idade = 23;
let cpf = "123.456.789-12";
let email = "tonimdosalio@email.com";
let telefone = "(34) 9 9999 5555";

console.log(`Que bom que você decidiu compartilhar seus dados pessoais, ${nome}. Vejo
aqui que você tem ${idade} anos de idade e seu CPF é ${cpf}. Nossa rede de 3498
lojas entrarão em contato com você pelo telefone ${telefone} para vender inúmeros
produtos de qualidade e que certamente você deseja ter em sua casa. Deseja receber
mais informações promocionais no seu e-mail “${email}”? Para que
perguntar, não é? Vou enviar mesmo assim.`)