let x = 5;
let y = 15;

console.log(`Para os valores de entrada '${x}' e '${y}' temos que:`);
console.log(`${x} + ${y} =`, x+y);
console.log(`${x} - ${y} =`, x-y);
console.log(`${x} * ${y} =`, x*y);
console.log(`${x} / ${y} =`, x/y);
