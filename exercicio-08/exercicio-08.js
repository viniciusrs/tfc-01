const player1 = "Gobbi";
const player2 = "M4rwic";
const arma1 = "arco e flechas";
const arma2 = "florete";
let forca1 = 96;
let forca2 = 61;

switch (arma1) {
  case "martelo de madeira":
    forca1 = (Math.random() * 20 + 16) * forca1 - 100 * 2;
    break;

  case "florete":
    forca1 = (Math.random() * 20 + 10) * forca1 - 100 * 7;
    break;

  case "arco e flechas":
    forca1 = (Math.random() * 20 + 20) * forca1 - 100 * 0;
    console.log(forca1);
    break;

  case "varinha de inverno":
    forca1 = (Math.random() * 20 + 17) * forca1 - 100 * 1;
    break;
}

switch (arma2) {
  case "martelo de madeira":
    forca2 = (Math.random() * 20 + 16) * forca2 - 100 * 2;
    break;

  case "florete":
    forca2 = (Math.random() * 20 + 10) * forca2 - 100 * 7;
    console.log(forca2);
    break;

  case "arco e flechas":
    forca2 = (Math.random() * 20 + 20) * forca2 - 100 * 0;
    break;

  case "varinha de inverno":
    forca2 = (Math.random() * 20 + 17) * forca2 - 100 * 1;
    break;
}

console.log(
  `${player1} perdeu ${forca2.toFixed(
    0
  )} pontos de vida no ataque de ${player2}`
);

console.log(
  `${player2} perdeu ${forca1.toFixed(
    0
  )} pontos de vida no ataque de ${player1}`
);

if (forca2 > 1800 && forca1 > 1800) {
  console.log(`Todos morreram`);
}

if (forca2 < 1800 && forca1 < 1800) {
  console.log(`Todos sobreviveram`);
}

if (forca2 > 1800 && forca1 < 1800) {
  console.log(`${player1} morreu`);
}

if (forca2 < 1800 && forca1 > 1800) {
  console.log(`${player2} morreu`);
}
