var alunos = [];

const main = () => {
  const x = confirm("Deseja cadastrar alunos no sistema?");
  if (x === true) {
    cadastro();
  }
};

const validar_alunos = () => {
  const filtrados = alunos.filter((val) => val.nota < 60);
  return filtrados;
};

const cadastro = () => {
  const nome_aluno = prompt("Informe o nome do aluno");
  const nota_aluno = parseFloat(prompt("Informe a nota do aluno"));

  const aluno = {
    nome: nome_aluno,
    nota: nota_aluno,
  };

  alunos.push(aluno);

  const y = confirm("Deseja cadastrar mais alunos?");

  if (y === true) {
    cadastro();
  } else {
    const filtrados = validar_alunos();
    let resposta = "Alunos reprovados: \n";
    for (const item of filtrados) {
      resposta =
        resposta +
        `
        Nome: ${item.nome} | Nota: ${item.nota}
        `;
    }
    console.log(resposta);
  }
};

main();
