let w = prompt("Informe a opção desejada (ASC, DESC, INTER): ");

while (w != "ASC" && w != "DESC" && w != "INTER") {
  alert("Opção incorreta!");
  w = prompt("Informe a opção desejada (ASC, DESC, INTER): ");
  console.log(w);
}

if (w === "ASC") {
  let x = 1;
  while (x <= 10) {
    console.log(x);
    x++;
  }
}

if (w === "DESC") {
  let x = 10;
  while (x >= 1) {
    console.log(x);
    x--;
  }
}

if (w === "INTER") {
  let x = 1;
  let y = 10;
  while (x <= 10) {
    console.log(x);
    x++;
    console.log(y);
    y--;
  }
}
