let nota_aluno1 = 90.8;
let nota_aluno2 = 50.9;
let nota_aluno3 = 70.2;
let media = (nota_aluno1 + nota_aluno2 + nota_aluno3) / 3

console.log(`O aluno 1 teve um aproveitamento de: ${nota_aluno1}
O aluno 2 teve um aproveitamento de: ${nota_aluno2}
O aluno 3 teve um aproveitamento de: ${nota_aluno3}
A média da nota final de todos os alunos que frequentaram o presencial foi de: ${media.toFixed(2)}`)