const nota1 = 12.3;
const nota2 = 22.5;
const nota3 = 2.8;
const nota4 = 21.6;
const soma = nota1 + nota2 + nota3 + nota4;

if(soma >= 60){
    console.log(`O aluno foi aprovado com nota igual a ${soma.toFixed(2)}`);
}

else{
    console.log(`O aluno foi reprovado, faltando ${(60-soma).toFixed(2)} pontos para a aprovação`);
}



