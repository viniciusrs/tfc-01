let a = parseInt(prompt("Informe o valor de A: "));
let b = parseInt(prompt("Informe o valor de B: "));

let w = prompt("Informe a opção desejada (SUM, MUL, DIV): ");
let resp = 0;

while (w != "SUM" && w != "MUL" && w != "DIV") {
  alert("Opção incorreta!");
  w = prompt("Informe a opção desejada (SUM, MUL, DIV): ");
}

if (w === "SUM") {
  while (a <= b) {
    resp = resp + a;
    a++;
  }
}

if (w === "MUL") {
  resp = 1;
  while (a <= b) {
    resp = resp * a;
    a++;
  }
}

if (w === "DIV") {
  resp = a;

  while (a < b) {
    resp = resp / (a + 1);
    a++;
  }
}
console.log(`${w} = ${resp}`);
