var alunos = [];

const main = () => {
  const x = confirm("Deseja cadastrar alunos no sistema?");
  if (x === true) {
    cadastro();
  }
};

const validar_alunos = () => {
  alunos.sort((a, b) => (a.nota < b.nota ? 1 : b.nota < a.nota ? -1 : 0));
  const filtrados = alunos.filter((val) => val.nota < 60);

  return filtrados;
};

const reajuste = () => {
  alunos.map((item) => {
    if (item.bonus === true && item.nota >= 55 && item.nota < 60) {
      item.nota = 60;
    }
    if (item.bonus === true && item.nota > 60) {
      item.nota = item.nota + item.nota * 0.1;
      if (item.nota > 100) {
        item.nota = 100;
      }
    }
    return item;
  });
};

const cadastro = () => {
  const nome_aluno = prompt("Informe o nome do aluno");
  const nota_aluno = parseFloat(prompt("Informe a nota do aluno"));
  const bonus_aluno = confirm("O aluno possui bônus na nota?");

  const aluno = {
    nome: nome_aluno,
    nota: nota_aluno,
    bonus: bonus_aluno,
  };

  alunos.push(aluno);

  const y = confirm("Deseja cadastrar mais alunos?");

  if (y === true) {
    cadastro();
  } else {
    reajuste();

    const filtrados = validar_alunos();

    const total = alunos.reduce((soma, aluno) => soma + aluno.nota, 0);
    const media = total / alunos.length;

    let resposta = "Todos os alunos: \n";

    for (const item of alunos) {
      resposta =
        resposta +
        `
            Nome: ${item.nome} | Nota: ${item.nota}
        `;
    }
    resposta =
      resposta +
      `
Alunos reprovados:
            `;
    for (const item of filtrados) {
      resposta =
        resposta +
        `
            Nome: ${item.nome} | Nota: ${item.nota}
        `;
    }
    resposta =
      resposta +
      `
Media de notas: ${media.toFixed(2)}
      `;

    console.log(resposta);
  }
};

main();
