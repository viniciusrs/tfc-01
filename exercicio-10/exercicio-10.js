const x = parseInt(prompt("Informe um numero:"));

while (x === "NaN") {
  const x = parseInt(prompt("Valor invalido! Informe novamente:"));
}

const y = prompt("Informe um operador(+ - / *)");

while (y != "+" && y != "-" && y != "/" && y != "*") {
  const y = prompt("Operador invalido \n Informe novamente(+ - / *)");
}

const z = parseInt(prompt("Informe um numero:"));

while (z === "NaN") {
  const z = parseInt(prompt("Valor invalido! Informe novamente:"));
}

if (y === "+") {
  console.log(`${x} + ${z} = ${x + z}`);
} else if (y === "-") {
  console.log(`${x} - ${z} = ${x - z}`);
} else if (y === "/") {
  console.log(`${x} / ${z} = ${x / z}`);
} else if (y === "*") {
  console.log(`${x} * ${z} = ${x * z}`);
}
